#include <stdio.h>
#include <stdlib.h>

#define MAXVERTICES 100 /*maximo numero de nodos*/

typedef struct {
    char nombre[50];

} tipovertice;

struct tipografo {
    int N; //número de vértices del grafo
    tipovertice VERTICES[MAXVERTICES]; //vector de vértices
    int A[MAXVERTICES][MAXVERTICES]; //matriz de adyacencia
};

typedef struct tipografo * grafo;

//Creación del grafo con 0 nodos

void inicializar(grafo *G) {
    *G = (struct tipografo*) malloc(sizeof (struct tipografo));
    (*G)->N = 0;
}

//ESTA FUNCIÓN HAY QUE MODIFICARLA EN grafo.c SI SE CAMBIA EL TIPO DE DATO tipovertice
//PARA QUE TENGA EN CUENTA tipovertice A LA HORA DE BUSCAR LA POSICIÓN Vert en el vector VERTICES
//Devuelve la posición del vértice Vert en el vector VERTICES del grafo G
//Si devuelve -1 es porque no encontró el vértice

int posicion(grafo G, tipovertice V) {
    int contador, p;
    p = -1;
    contador = 0;
    while (contador < G->N && p < 0) {
        //comparo V con todos los vertices almacenados en VERTICES        
        if (strcmp((G->VERTICES[contador]).nombre, V.nombre) == 0) //encontré la posicion de V
            p = contador;
        contador++;
    }
    return p;
}

//Devuelve 1 si el grafo G existe y 0 en caso contrario

int existe(grafo G) {
    return (G != NULL);
}

//Devuelve 1 si el vértice Vert existe en el grafo G

int existeVertice(grafo G, tipovertice V) {
    return (posicion(G, V) >= 0);
}

//Inserta un vértice en el grafo

void nuevoVertice(grafo *G, tipovertice Vert) {
    int i;
    if ((*G)->N < MAXVERTICES) {
        ((*G)->N)++;
        (*G)->VERTICES[((*G)->N) - 1] = Vert;
        for (i = 0; i < (*G)->N; i++) {
            (*G)->A[i][((*G)->N) - 1] = 0;
            (*G)->A[((*G)->N) - 1][i] = 0;
        }
    } else
        printf("Grafo lleno!\n");
}

//Borra un vértice del grafo

void borrarVertice(grafo *G, tipovertice Vert) {
    int F, C, P;
    P = posicion((*G), Vert);
    if (P >= 0 && P < (*G)->N) {
        for (F = P; F < ((*G)->N) - 1; F++)
            (*G)->VERTICES[F] = (*G)->VERTICES[F + 1];

        for (C = P; C < ((*G)->N) - 1; C++)
            for (F = 0; F < (*G)->N; F++)
                (*G)->A[F][C] = (*G)->A[F][C + 1];

        for (F = P; F < ((*G)->N) - 1; F++)
            for (C = 0; C < (*G)->N; C++)
                (*G)->A[F][C] = (*G)->A[F + 1][C];

        ((*G)->N)--;
    } else
        printf("Vertice inexistente!\n");
}

//Crea el arco de relación entre VERTICES(pos1) y VERTICES(pos2)

void nuevoArco(grafo *G, int pos1, int pos2) {
    (*G)->A[pos1][pos2] = 1;
}

//Borra el arco de relación entre VERTICES(pos1) y VERTICES(pos2)

void borrarArco(grafo *G, int pos1, int pos2) {
    (*G)->A[pos1][pos2] = 0;
}

//Devuelve 1 si VERTICES(pos1) y VERTICES(pos2) son vértices adyacentes

int adyacente(grafo G, int pos1, int pos2) {
    return (G->A[pos1][pos2]);
}

//Destruye el grafo

void borrarGrafo(grafo *G) {
    free(*G);
    *G = NULL;
}

//Devuelve el número de vértices del grafo G

int infoN(grafo G) {
    return G->N;
}

//Devuelve el vector de vértices VERTICES del grafo G

tipovertice* infovertices(grafo G) {
    return G->VERTICES;
}
