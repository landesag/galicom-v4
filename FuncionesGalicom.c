
#include "abb.h"

//Esta funcion vale para introducir rapidamente en un empleado los campos de una nueva URL//

void IntroduceUrl(tipoelem *empleado, tipoclave urll, tipoclave fechaa, tipoclave horaa) {

    tipoelempila datos;
    strcpy(datos.web, urll);
    strcpy(datos.fecha, fechaa);
    strcpy(datos.hora, horaa);
    push(&(empleado->url), datos);
}

//Esta funcion vale par imprimir un empleado y hasta 1000 urls de su pila//

void ImprimeEmpleado(tipoelem empleado) {


    printf("Nombre: %s\n", empleado.nombre);
    printf("NIF: %s\n", empleado.nif);
    printf("Urls visitadas: \n");
    imprimePila(empleado.url, 1000);

}


//Esta funcion vale par imprimir un empleado y, de su pila, un numero determinado de urls//

void ImprimeEmpleadoAvanzado(tipoelem empleado, int n) {

    printf("Nombre: %s\n", empleado.nombre);
    printf("NIF: %s\n", empleado.nif);
    printf("Ultimas %d Urls: \n", n);
    imprimePila(empleado.url, n);
}

//Esta funcion se usa para imprimir la lista completa de empleados y sus pilas de url (no//
//se pedia pero la implemente por su utilidad)//

void inordengali(abb A) {
    tipoelem E;
    if (!esVacio(A)) {
        inordengali(izq(A));
        info(A, &E);
        printf("---\n");
        printf("Nombre: %s\n", E.nombre);
        printf("Nif: %s\n", E.nif);
        printf("Urls visitadas: \n");
        imprimePila(E.url, 10);
        inordengali(der(A));
    }
}

//Esta funcion sirve para determinar que empleados visitaron la web de la competencia//

void inordenurl(abb A) {
    char urll[] = "micuenta.bancobancario.com";
    int band = 0;
    tipoelem E;
    pila aux;
    tipoelempila a;
    creaPila(&aux);
    if (!esVacio(A)) {
        inordenurl(izq(A));
        info(A, &E);
        while (!esVaciaP(E.url)) {
            a = tope(E.url);
            if (strcmp(a.web, urll) == 0 && band == 0) {
                printf("---\n");
                printf("Nombre: %s\n", E.nombre);
                printf("Nif: %s\n", E.nif);
                band = 1;
            }
            push(&aux, a);
            pop(&(E.url));
        }
        while (!esVaciaP(aux)) {
            push(&(E.url), (tope(aux)));
            pop(&aux);
        }
        destruyePila(&aux);
        band = 0;
        inordenurl(der(A));
    }
}

//Esta funcion sirve para cargar los empleados de un archivo en un arbol para que el programa//
//pueda trabajar con ellos//

void CargaArchivo(FILE *arc, abb *A) {


    tipoelem aux2;
    char aux[30];
    tipoelempila a;
    int band = 0;


    if (arc == NULL) {
        printf("No hay archivo.\n");

    } else {

        do {
            band = 0;
            creaPila(&(aux2.url));
            fscanf(arc, "%50[^\r\n]", aux2.nombre);
            fscanf(arc, "%s", aux2.nif);



            while (band == 0) {
                fscanf(arc, "%s", aux);
                if ((strcmp("***", aux)) == 0) {
                    fscanf(arc, "\r\n", aux);
                    band = 1;
                } else {
                    strcpy(a.web, aux);
                    fscanf(arc, "%s", a.fecha);
                    fscanf(arc, "%s", a.hora);
                    push(&(aux2.url), a);
                }
            }

            inserta(A, aux2);

        } while (!feof(arc));

        fclose(arc);
    }
}


//Esta funcion se usa para introducir un empleado nuevo si su nombre no esta en el arbol//
//o para actualizar su pila de url si el empleado ya esta en el arbol//

void Intro(abb *A, char apnom[]) {
    tipovertice aux;
    tipoelem aux2;
    char url[30], fecha[30], hora[30];

    strcpy(aux2.nombre, apnom);

    if (esMiembro(*A, aux2) == 1) {
        printf("El usuario ya esta registrado, introduce la nueva url:\n");
        buscanodo(*A, apnom, &aux2);

        gets(url);

        printf("Introduce fecha de la url: ");
        gets(fecha);
        printf("Introduce hora de la url: ");
        gets(hora);
        IntroduceUrl(&aux2, url, fecha, hora);

        modifica(A, apnom, aux2);
        printf("Modificado.\n\n");

        printf("----Empleados----\n");
        inordengali(*A);
    } else {

        printf("Usuario no registrado.\n");

        printf("Introduce nif:\n");
        gets(aux2.nif);


        creaPila(&(aux2.url));
        printf("Introduce url: ");
        gets(url);
        printf("Introduce fecha de la url: ");
        gets(fecha);
        printf("Introduce hora de la url: ");
        gets(hora);

        IntroduceUrl(&aux2, url, fecha, hora);


        inicializar(&aux2.amigos);
        strcpy(aux.nombre, aux2.nombre);
        nuevoVertice(&(aux2.amigos), aux);

        inserta(A, aux2);

        printf("\n");
        printf("----Empleados----");
        printf("\n");
        inordengali(*A);
        printf("\n");
    }
}

//Esta función se usa para eliminar un empleado de los grafos de sus amigos//

void EliminaAmigos(abb *A, tipoelem elim) {
    tipoelem E;
    abb izquierda, derecha;
    tipovertice vertelim, vertaux;
    strcpy(vertelim.nombre, elim.nombre);
    if (!esVacio(*A)) {
        izquierda = izq(*A);
        EliminaAmigos(&izquierda, elim);
        info(*A, &E);
        strcpy(vertaux.nombre, E.nombre);
        if (existeVertice(E.amigos, vertelim)) {
            borrarArco(&E.amigos, posicion(E.amigos, vertaux), posicion(E.amigos, vertelim));
            borrarVertice(&E.amigos, vertelim);
            modifica(A, E.nombre, E);
        }
        derecha = der(*A);
        EliminaAmigos(&derecha, elim);
    }
}

//Esta funcion se usa para eliminar un empleado del grafo global//

void EliminaGlobal(grafo *G, tipoelem elim) {

    tipovertice vertelim;
    tipovertice *vertices;
    int N, i;
    N = infoN(*G);
    strcpy(vertelim.nombre, elim.nombre);
    vertices = infovertices(*G);

    if (existeVertice(*G, vertelim)) {
        for (i = 0; i < N; i++) {
            if (adyacente(*G, posicion(*G, vertelim), posicion(*G, vertices[i]))) {
                borrarArco(G, posicion(*G, vertelim), posicion(*G, vertices[i]));
            }
            if (adyacente(*G, posicion(*G, vertices[i]), posicion(*G, vertelim))) {
                borrarArco(G, posicion(*G, vertices[i]), posicion(*G, vertelim));
            }
        }
        borrarVertice(G, vertelim);
    }
}

//Esta funcion sirve para eliminar un empleado del arbol introduciendo su nombre//

void Del(abb *A, grafo *G, char apnom[]) {

    tipoelem aux;
    strcpy(aux.nombre, apnom);

    EliminaAmigos(A, aux);
    EliminaGlobal(G, aux);
    suprime(A, aux);

    inordengali(*A);
}



//Esta funcion sirve para imprimir por pantalla una pila cuyo tipoelempila es un struct//
//con tres campos: web, fecha y hora//

void imprimePila(pila P, int numurl) {

    pila aux;
    tipoelempila a;
    creaPila(&aux);


    while (!esVaciaP(P) && (numurl != 0)) {
        a = tope(P);
        printf("-%s  ", a.web);
        printf("%s  ", a.fecha);
        printf("%s\n", a.hora);

        push(&aux, a);
        pop(&P);
        numurl = numurl - 1;
    }
    while (!esVaciaP(aux)) {

        push(&P, (tope(aux)));
        pop(&aux);
    }
    destruyePila(&aux);
}

//Esta funcion se usa para calcular cual es el empleado que navego mas//

void navegadorDelMes(tipoelem *navegMes, int *maxim, abb A) {

    tipoelem E, I;
    pila aux;
    int i = 0;
    tipoelempila a;

    creaPila(&aux);

    if (!esVacio(A)) {

        navegadorDelMes(navegMes, maxim, izq(A));

        info(A, &E);

        while (!esVaciaP(E.url)) {
            i++;
            a = tope(E.url);
            pop(&E.url);
            push(&aux, a);
        }

        while (!esVaciaP(aux)) {

            push(&(E.url), (tope(aux)));
            pop(&aux);
        }
        destruyePila(&aux);
        if (i > (*maxim)) {
            *maxim = i;
            *navegMes = E;
        }

        modifica(&A, E.nombre, E);
        navegadorDelMes(navegMes, maxim, der(A));
    }

}

//Esta funcion se usa para imprimir un arbol de empleados en un archivo//

void imprimeEnArchivo(FILE *arc, abb A) {


    tipoelem E;
    pila aux;
    tipoelempila a;


    if (!esVacio(A)) {
        imprimeEnArchivo(arc, izq(A));
        info(A, &E);
        printf("---\n");
        fprintf(arc, "%s\n", E.nombre);
        fprintf(arc, "%s\n", E.nif);
        while (!esVaciaP(E.url)) {
            a = tope(E.url);
            fprintf(arc, "%s ", a.web);
            fprintf(arc, "%s ", a.fecha);
            fprintf(arc, "%s\n", a.hora);

            pop(&E.url);

        }
        fprintf(arc, "***\n");

        imprimeEnArchivo(arc, der(A));
    }


}

//Esta función se usa para guardar un grafo en un archivo//

void imprimeGrafo(FILE *arc, grafo G) {

    tipovertice *VECTOR;
    int N;

    N = infoN(G);
    VECTOR = infovertices(G);

    int i, j;

    for (i = 0; i < N; i++) {

        fprintf(arc, "%s\n", VECTOR[i].nombre);

        for (j = 0; j < N; j++) {
            if (adyacente(G, i, j))
                fprintf(arc, "%s\n", VECTOR[j].nombre);
        }
        fprintf(arc, "***\n");
    }

}

//Esta función se usa para cargar un grafo de un archivo en memoria//

void CargaGrafo(FILE *arc, grafo *G, abb *A) {

    tipovertice aux, este;
    tipoelem E;
    char aux2[50];
    char aux3[50];
    int band = 0;


    if (arc == NULL) {
        printf("No hay archivo para el grafo de empleados.\n");

    } else {

        do {
            band = 0;

            fscanf(arc, "%50[^\r\n]", este.nombre);
            fscanf(arc, "\r\n", aux2);
            buscanodo(*A, este.nombre, &E);
            inicializar(&E.amigos);
            nuevoVertice(&E.amigos, este);

            if (!existeVertice(*G, este)) {
                nuevoVertice(G, este);
            }
            while (band == 0) {
                fscanf(arc, "%50[^\r\n]", aux2);
                fscanf(arc, "\r\n", aux3);
                if ((strcmp("***", aux2)) == 0) {
                    fscanf(arc, "\r\n", aux2);
                    band = 1;
                } else {
                    strcpy(aux.nombre, aux2);
                    nuevoVertice(&E.amigos, aux);
                    nuevoArco(&E.amigos, posicion(E.amigos, este), posicion(E.amigos, aux));
                    if (!existeVertice(*G, aux)) {
                        nuevoVertice(G, aux);
                        nuevoArco(G, posicion(*G, este), posicion(*G, aux));
                    } else {
                        nuevoArco(G, posicion(*G, este), posicion(*G, aux));
                    }

                }
            }
            modifica(A, este.nombre, E);


        } while (!feof(arc));

        fclose(arc);

    }
}

//Esta función se usa para introducir un amigo//

void IntroduceAmigo(abb A, tipoelem *empleadoActual, char apnom[]) {
    tipovertice verticeEmpleado;
    tipoelem aux;
    tipovertice vertice2;
    strcpy(verticeEmpleado.nombre, (*empleadoActual).nombre);
    strcpy(aux.nombre, apnom);
    if (esMiembro(A, aux)) {

        strcpy(vertice2.nombre, apnom);

        if (!existeVertice((*empleadoActual).amigos, vertice2)) {

            nuevoVertice(&((*empleadoActual).amigos), vertice2);
            nuevoArco(&((*empleadoActual).amigos), posicion((*empleadoActual).amigos, verticeEmpleado), posicion((*empleadoActual).amigos, vertice2));

        } else {
            printf("Ese amigo ya esta en el grafo.\n");
        }

    } else {

        printf("Ese empleado no esta en el arbol.\n");
    }


}

//Esta función se usa para eliminar un amigo del grafo de un empleado//

void EliminaAmigo(abb A, tipoelem *empleadoActual, char apnom[]) {

    tipoelem aux;
    tipovertice vertice2;
    tipovertice verticeEmpleado;
    strcpy(verticeEmpleado.nombre, (*empleadoActual).nombre);

    strcpy(aux.nombre, apnom);
    if (esMiembro(A, aux)) {

        strcpy(vertice2.nombre, apnom);

        if (existeVertice((*empleadoActual).amigos, vertice2)) {

            borrarArco(&((*empleadoActual).amigos), posicion((*empleadoActual).amigos, verticeEmpleado), posicion((*empleadoActual).amigos, vertice2));
            borrarVertice(&((*empleadoActual).amigos), vertice2);

        } else {
            printf("Ese empleado no esta en el grafo de amigos.\n");
        }

    } else {

        printf("Ese empleado no esta en el arbol.\n");
    }


}

//Esta función actualiza el grafo global//

void ActualizaGlobal(grafo *G, abb A) {
    int i;
    tipoelem E;
    tipovertice vertice;
    tipovertice *vertices;


    if (!esVacio(A)) {

        ActualizaGlobal(G, izq(A));

        info(A, &E);

        strcpy(vertice.nombre, E.nombre);

        if (!existeVertice(*G, vertice)) {
            nuevoVertice(G, vertice);
        }

        vertices = infovertices(E.amigos);

        for (i = 0; i < infoN(E.amigos); i++) {
            if (strcmp(E.nombre, vertices[i].nombre) != 0) {
                if (!existeVertice(*G, vertices[i])) {
                    nuevoVertice(G, vertices[i]);
                    nuevoArco(G, posicion(*G, vertice), posicion(*G, vertices[i]));
                } else {
                    nuevoArco(G, posicion(*G, vertice), posicion(*G, vertices[i]));
                }
            }
        }

        ActualizaGlobal(G, der(A));
    }
}

//Esta función determina cual es el empleado mas popular//

void empleadoPopular(grafo GLOBAL, tipovertice *aux) {


    int numVert, i, j, max = 0, contador;
    tipovertice * empleados;
    numVert = infoN(GLOBAL);
    empleados = infovertices(GLOBAL);

    for (i = 0; i < numVert; i++) {

        contador = 0;

        for (j = 0; j < numVert; j++) {

            if (adyacente(GLOBAL, posicion(GLOBAL, empleados[j]), posicion(GLOBAL, empleados[i]))) {
                contador++;
            }
            if (contador > max) {
                max = contador;
                *aux = empleados[i];
            }
        }

    }

}
