#include <stdlib.h>

//////////////////////////////////////////////////

struct datosUrl {
    char web[30];
    char fecha[30];
    char hora[30];
};

typedef struct datosUrl tipoelempila;
//////////////////////////////////////////////////
typedef void *pila; /*tipo opaco*/

void creaPila(pila *P);
void destruyePila(pila *P);
unsigned esVaciaP(pila P);
tipoelempila tope(pila P);
void push(pila *P, tipoelempila E);
void pop(pila *P);
void imprimePila(pila P, int numurl);