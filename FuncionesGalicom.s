	.file	"FuncionesGalicom.c"
	.text
	.globl	IntroduceUrl
	.type	IntroduceUrl, @function
IntroduceUrl:
.LFB2:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$240, %rsp
	movq	%rdi, -120(%rbp)
	movq	%rsi, -128(%rbp)
	movq	%rdx, -136(%rbp)
	movq	%rcx, -144(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-128(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-136(%rbp), %rax
	leaq	-112(%rbp), %rdx
	addq	$30, %rdx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	strcpy
	movq	-144(%rbp), %rax
	leaq	-112(%rbp), %rdx
	addq	$60, %rdx
	movq	%rax, %rsi
	movq	%rdx, %rdi
	call	strcpy
	movq	-120(%rbp), %rax
	leaq	72(%rax), %rdx
	movq	-112(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsp)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsp)
	movq	-56(%rbp), %rax
	movq	%rax, 56(%rsp)
	movq	-48(%rbp), %rax
	movq	%rax, 64(%rsp)
	movq	-40(%rbp), %rax
	movq	%rax, 72(%rsp)
	movq	-32(%rbp), %rax
	movq	%rax, 80(%rsp)
	movzwl	-24(%rbp), %eax
	movw	%ax, 88(%rsp)
	movq	%rdx, %rdi
	call	push
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L2
	call	__stack_chk_fail
.L2:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE2:
	.size	IntroduceUrl, .-IntroduceUrl
	.section	.rodata
.LC0:
	.string	"Nombre: %s\n"
.LC1:
	.string	"NIF: %s\n"
.LC2:
	.string	"Urls visitadas: "
	.text
	.globl	ImprimeEmpleado
	.type	ImprimeEmpleado, @function
ImprimeEmpleado:
.LFB3:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	leaq	16(%rbp), %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	66(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	$.LC2, %edi
	call	puts
	movq	88(%rbp), %rax
	movl	$1000, %esi
	movq	%rax, %rdi
	call	imprimePila
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE3:
	.size	ImprimeEmpleado, .-ImprimeEmpleado
	.section	.rodata
.LC3:
	.string	"Ultimas %d Urls: \n"
	.text
	.globl	ImprimeEmpleadoAvanzado
	.type	ImprimeEmpleadoAvanzado, @function
ImprimeEmpleadoAvanzado:
.LFB4:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$16, %rsp
	movl	%edi, -4(%rbp)
	leaq	16(%rbp), %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	66(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC1, %edi
	movl	$0, %eax
	call	printf
	movl	-4(%rbp), %eax
	movl	%eax, %esi
	movl	$.LC3, %edi
	movl	$0, %eax
	call	printf
	movq	88(%rbp), %rax
	movl	-4(%rbp), %edx
	movl	%edx, %esi
	movq	%rax, %rdi
	call	imprimePila
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE4:
	.size	ImprimeEmpleadoAvanzado, .-ImprimeEmpleadoAvanzado
	.section	.rodata
.LC4:
	.string	"---"
.LC5:
	.string	"Nif: %s\n"
	.text
	.globl	inordengali
	.type	inordengali, @function
inordengali:
.LFB5:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$112, %rsp
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-104(%rbp), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L5
	movq	-104(%rbp), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, %rdi
	call	inordengali
	leaq	-96(%rbp), %rdx
	movq	-104(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	movl	$.LC4, %edi
	call	puts
	leaq	-96(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-96(%rbp), %rax
	addq	$50, %rax
	movq	%rax, %rsi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	$.LC2, %edi
	call	puts
	movq	-24(%rbp), %rax
	movl	$10, %esi
	movq	%rax, %rdi
	call	imprimePila
	movq	-104(%rbp), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, %rdi
	call	inordengali
.L5:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L7
	call	__stack_chk_fail
.L7:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE5:
	.size	inordengali, .-inordengali
	.globl	inordenurl
	.type	inordenurl, @function
inordenurl:
.LFB6:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$560, %rsp
	movq	%rdi, -360(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movabsq	$7022359101017647469, %rax
	movq	%rax, -336(%rbp)
	movabsq	$7017293642240975406, %rax
	movq	%rax, -328(%rbp)
	movabsq	$7146772157364790126, %rax
	movq	%rax, -320(%rbp)
	movw	$28015, -312(%rbp)
	movb	$0, -310(%rbp)
	movl	$0, -348(%rbp)
	leaq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	creaPila
	movq	-360(%rbp), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L8
	movq	-360(%rbp), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, %rdi
	call	inordenurl
	leaq	-304(%rbp), %rdx
	movq	-360(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	jmp	.L10
.L12:
	movq	-232(%rbp), %rdx
	leaq	-464(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	movq	-464(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-456(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-448(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-440(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-432(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-424(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-416(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-408(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-400(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-392(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-384(%rbp), %rax
	movq	%rax, -128(%rbp)
	movzwl	-376(%rbp), %eax
	movw	%ax, -120(%rbp)
	leaq	-336(%rbp), %rdx
	leaq	-208(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	jne	.L11
	cmpl	$0, -348(%rbp)
	jne	.L11
	movl	$.LC4, %edi
	call	puts
	leaq	-304(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC0, %edi
	movl	$0, %eax
	call	printf
	leaq	-304(%rbp), %rax
	addq	$50, %rax
	movq	%rax, %rsi
	movl	$.LC5, %edi
	movl	$0, %eax
	call	printf
	movl	$1, -348(%rbp)
.L11:
	leaq	-344(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-192(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-184(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-176(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-128(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movzwl	-120(%rbp), %edx
	movw	%dx, 88(%rsp)
	movq	%rax, %rdi
	call	push
	leaq	-304(%rbp), %rax
	addq	$72, %rax
	movq	%rax, %rdi
	call	pop
.L10:
	movq	-232(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L12
	jmp	.L13
.L14:
	movq	-344(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	leaq	-304(%rbp), %rax
	leaq	72(%rax), %rdx
	movq	-112(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsp)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsp)
	movq	-56(%rbp), %rax
	movq	%rax, 56(%rsp)
	movq	-48(%rbp), %rax
	movq	%rax, 64(%rsp)
	movq	-40(%rbp), %rax
	movq	%rax, 72(%rsp)
	movq	-32(%rbp), %rax
	movq	%rax, 80(%rsp)
	movzwl	-24(%rbp), %eax
	movw	%ax, 88(%rsp)
	movq	%rdx, %rdi
	call	push
	leaq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	pop
.L13:
	movq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L14
	leaq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	destruyePila
	movl	$0, -348(%rbp)
	movq	-360(%rbp), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, %rdi
	call	inordenurl
.L8:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L15
	call	__stack_chk_fail
.L15:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE6:
	.size	inordenurl, .-inordenurl
	.section	.rodata
.LC6:
	.string	"No hay archivo."
.LC7:
	.string	"%50[^\r\n]"
.LC8:
	.string	"%s"
.LC9:
	.string	"***"
.LC10:
	.string	"\r\n"
	.text
	.globl	CargaArchivo
	.type	CargaArchivo, @function
CargaArchivo:
.LFB7:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$368, %rsp
	movq	%rdi, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -244(%rbp)
	cmpq	$0, -264(%rbp)
	jne	.L17
	movl	$.LC6, %edi
	call	puts
	jmp	.L16
.L17:
	movl	$0, -244(%rbp)
	leaq	-208(%rbp), %rax
	addq	$72, %rax
	movq	%rax, %rdi
	call	creaPila
	leaq	-208(%rbp), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC7, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-208(%rbp), %rax
	leaq	50(%rax), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC8, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	jmp	.L19
.L21:
	leaq	-240(%rbp), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC8, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-240(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC9, %edi
	call	strcmp
	testl	%eax, %eax
	jne	.L20
	leaq	-240(%rbp), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC10, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	movl	$1, -244(%rbp)
	jmp	.L19
.L20:
	leaq	-240(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	leaq	-112(%rbp), %rax
	leaq	30(%rax), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC8, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-112(%rbp), %rax
	leaq	60(%rax), %rdx
	movq	-264(%rbp), %rax
	movl	$.LC8, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-208(%rbp), %rax
	leaq	72(%rax), %rdx
	movq	-112(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsp)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsp)
	movq	-56(%rbp), %rax
	movq	%rax, 56(%rsp)
	movq	-48(%rbp), %rax
	movq	%rax, 64(%rsp)
	movq	-40(%rbp), %rax
	movq	%rax, 72(%rsp)
	movq	-32(%rbp), %rax
	movq	%rax, 80(%rsp)
	movzwl	-24(%rbp), %eax
	movw	%ax, 88(%rsp)
	movq	%rdx, %rdi
	call	push
.L19:
	cmpl	$0, -244(%rbp)
	je	.L21
	movq	-272(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-192(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-184(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-176(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-128(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	inserta
	movq	-264(%rbp), %rax
	movq	%rax, %rdi
	call	feof
	testl	%eax, %eax
	je	.L17
	movq	-264(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L16:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L22
	call	__stack_chk_fail
.L22:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE7:
	.size	CargaArchivo, .-CargaArchivo
	.section	.rodata
	.align 8
.LC11:
	.string	"El usuario ya esta registrado, introduce la nueva url:"
.LC12:
	.string	"Introduce fecha de la url: "
.LC13:
	.string	"Introduce hora de la url: "
.LC14:
	.string	"Modificado.\n"
.LC15:
	.string	"----Empleados----"
.LC16:
	.string	"Usuario no registrado."
.LC17:
	.string	"Introduce nif:"
.LC18:
	.string	"Introduce url: "
	.text
	.globl	Intro
	.type	Intro, @function
Intro:
.LFB8:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$368, %rsp
	movq	%rdi, -264(%rbp)
	movq	%rsi, -272(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-272(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-24(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-16(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	esMiembro
	cmpl	$1, %eax
	jne	.L24
	movl	$.LC11, %edi
	call	puts
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	leaq	-96(%rbp), %rdx
	movq	-272(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	buscanodo
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	movl	$.LC12, %edi
	movl	$0, %eax
	call	printf
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	movl	$.LC13, %edi
	movl	$0, %eax
	call	printf
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	leaq	-192(%rbp), %rcx
	leaq	-224(%rbp), %rdx
	leaq	-256(%rbp), %rsi
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	IntroduceUrl
	movq	-272(%rbp), %rdx
	movq	-264(%rbp), %rax
	movq	-96(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	-88(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movq	-80(%rbp), %rcx
	movq	%rcx, 16(%rsp)
	movq	-72(%rbp), %rcx
	movq	%rcx, 24(%rsp)
	movq	-64(%rbp), %rcx
	movq	%rcx, 32(%rsp)
	movq	-56(%rbp), %rcx
	movq	%rcx, 40(%rsp)
	movq	-48(%rbp), %rcx
	movq	%rcx, 48(%rsp)
	movq	-40(%rbp), %rcx
	movq	%rcx, 56(%rsp)
	movq	-32(%rbp), %rcx
	movq	%rcx, 64(%rsp)
	movq	-24(%rbp), %rcx
	movq	%rcx, 72(%rsp)
	movq	-16(%rbp), %rcx
	movq	%rcx, 80(%rsp)
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	modifica
	movl	$.LC14, %edi
	call	puts
	movl	$.LC15, %edi
	call	puts
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	inordengali
	jmp	.L23
.L24:
	movl	$.LC16, %edi
	call	puts
	movl	$.LC17, %edi
	call	puts
	leaq	-96(%rbp), %rax
	addq	$50, %rax
	movq	%rax, %rdi
	call	gets
	leaq	-96(%rbp), %rax
	addq	$72, %rax
	movq	%rax, %rdi
	call	creaPila
	movl	$.LC18, %edi
	movl	$0, %eax
	call	printf
	leaq	-256(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	movl	$.LC12, %edi
	movl	$0, %eax
	call	printf
	leaq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	movl	$.LC13, %edi
	movl	$0, %eax
	call	printf
	leaq	-192(%rbp), %rax
	movq	%rax, %rdi
	call	gets
	leaq	-192(%rbp), %rcx
	leaq	-224(%rbp), %rdx
	leaq	-256(%rbp), %rsi
	leaq	-96(%rbp), %rax
	movq	%rax, %rdi
	call	IntroduceUrl
	leaq	-96(%rbp), %rax
	addq	$80, %rax
	movq	%rax, %rdi
	call	inicializar
	leaq	-96(%rbp), %rdx
	leaq	-160(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	leaq	-96(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-160(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-152(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-144(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-136(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-128(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-120(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-112(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	nuevoVertice
	movq	-264(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-24(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-16(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	inserta
	movl	$10, %edi
	call	putchar
	movl	$.LC15, %edi
	movl	$0, %eax
	call	printf
	movl	$10, %edi
	call	putchar
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	inordengali
	movl	$10, %edi
	call	putchar
.L23:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L26
	call	__stack_chk_fail
.L26:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE8:
	.size	Intro, .-Intro
	.globl	EliminaAmigos
	.type	EliminaAmigos, @function
EliminaAmigos:
.LFB9:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	leaq	-240(%rbp), %rax
	leaq	16(%rbp), %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L27
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, -256(%rbp)
	leaq	-256(%rbp), %rax
	movq	16(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	24(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	32(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	40(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	64(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	72(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	80(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	88(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	96(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	EliminaAmigos
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	leaq	-112(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	leaq	-112(%rbp), %rdx
	leaq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-32(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-232(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-224(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-216(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-208(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-192(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	je	.L29
	movq	-32(%rbp), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-232(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-224(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-216(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-208(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-192(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-32(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	leaq	-112(%rbp), %rdx
	leaq	80(%rdx), %rcx
	movl	%ebx, %edx
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	borrarArco
	leaq	-112(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-240(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-232(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-224(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-216(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-208(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-200(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-192(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	borrarVertice
	leaq	-112(%rbp), %rdx
	movq	-264(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	-104(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rsp)
	movq	-88(%rbp), %rcx
	movq	%rcx, 24(%rsp)
	movq	-80(%rbp), %rcx
	movq	%rcx, 32(%rsp)
	movq	-72(%rbp), %rcx
	movq	%rcx, 40(%rsp)
	movq	-64(%rbp), %rcx
	movq	%rcx, 48(%rsp)
	movq	-56(%rbp), %rcx
	movq	%rcx, 56(%rsp)
	movq	-48(%rbp), %rcx
	movq	%rcx, 64(%rsp)
	movq	-40(%rbp), %rcx
	movq	%rcx, 72(%rsp)
	movq	-32(%rbp), %rcx
	movq	%rcx, 80(%rsp)
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	modifica
.L29:
	movq	-264(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, -248(%rbp)
	leaq	-248(%rbp), %rax
	movq	16(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	24(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	32(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	40(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	64(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	72(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	80(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	88(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	96(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	EliminaAmigos
.L27:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L30
	call	__stack_chk_fail
.L30:
	addq	$360, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE9:
	.size	EliminaAmigos, .-EliminaAmigos
	.globl	EliminaGlobal
	.type	EliminaGlobal, @function
EliminaGlobal:
.LFB10:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$168, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -104(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	infoN
	movl	%eax, -92(%rbp)
	leaq	-80(%rbp), %rax
	leaq	16(%rbp), %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	infovertices
	movq	%rax, -88(%rbp)
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	je	.L31
	movl	$0, -96(%rbp)
	jmp	.L33
.L36:
	movl	-96(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	-104(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	adyacente
	testl	%eax, %eax
	je	.L34
	movl	-96(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	-104(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-104(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	borrarArco
.L34:
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movl	-96(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	-104(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	adyacente
	testl	%eax, %eax
	je	.L35
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movl	-96(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-88(%rbp), %rax
	addq	%rdx, %rax
	movq	-104(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-104(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	borrarArco
.L35:
	addl	$1, -96(%rbp)
.L33:
	movl	-96(%rbp), %eax
	cmpl	-92(%rbp), %eax
	jl	.L36
	movq	-104(%rbp), %rax
	movq	-80(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-32(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	borrarVertice
.L31:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L37
	call	__stack_chk_fail
.L37:
	addq	$168, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE10:
	.size	EliminaGlobal, .-EliminaGlobal
	.globl	Del
	.type	Del, @function
Del:
.LFB11:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$224, %rsp
	movq	%rdi, -104(%rbp)
	movq	%rsi, -112(%rbp)
	movq	%rdx, -120(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-120(%rbp), %rdx
	leaq	-96(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-24(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-16(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	EliminaAmigos
	movq	-112(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-24(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-16(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	EliminaGlobal
	movq	-104(%rbp), %rax
	movq	-96(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-24(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-16(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	suprime
	movq	-104(%rbp), %rax
	movq	(%rax), %rax
	movq	%rax, %rdi
	call	inordengali
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L39
	call	__stack_chk_fail
.L39:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE11:
	.size	Del, .-Del
	.section	.rodata
.LC19:
	.string	"-%s  "
.LC20:
	.string	"%s  "
	.text
	.globl	imprimePila
	.type	imprimePila, @function
imprimePila:
.LFB12:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$432, %rsp
	movq	%rdi, -232(%rbp)
	movl	%esi, -236(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	leaq	-216(%rbp), %rax
	movq	%rax, %rdi
	call	creaPila
	jmp	.L41
.L43:
	movq	-232(%rbp), %rdx
	leaq	-336(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	movq	-336(%rbp), %rax
	movq	%rax, -208(%rbp)
	movq	-328(%rbp), %rax
	movq	%rax, -200(%rbp)
	movq	-320(%rbp), %rax
	movq	%rax, -192(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -184(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -176(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -168(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -160(%rbp)
	movq	-280(%rbp), %rax
	movq	%rax, -152(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, -144(%rbp)
	movq	-264(%rbp), %rax
	movq	%rax, -136(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -128(%rbp)
	movzwl	-248(%rbp), %eax
	movw	%ax, -120(%rbp)
	leaq	-208(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC19, %edi
	movl	$0, %eax
	call	printf
	leaq	-208(%rbp), %rax
	addq	$30, %rax
	movq	%rax, %rsi
	movl	$.LC20, %edi
	movl	$0, %eax
	call	printf
	leaq	-208(%rbp), %rax
	addq	$60, %rax
	movq	%rax, %rdi
	call	puts
	leaq	-216(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-192(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-184(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-176(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-128(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movzwl	-120(%rbp), %edx
	movw	%dx, 88(%rsp)
	movq	%rax, %rdi
	call	push
	leaq	-232(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	subl	$1, -236(%rbp)
.L41:
	movq	-232(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	jne	.L47
	cmpl	$0, -236(%rbp)
	jne	.L43
	jmp	.L47
.L45:
	movq	-216(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	leaq	-232(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-104(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-96(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movzwl	-24(%rbp), %edx
	movw	%dx, 88(%rsp)
	movq	%rax, %rdi
	call	push
	leaq	-216(%rbp), %rax
	movq	%rax, %rdi
	call	pop
	jmp	.L44
.L47:
	nop
.L44:
	movq	-216(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L45
	leaq	-216(%rbp), %rax
	movq	%rax, %rdi
	call	destruyePila
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L46
	call	__stack_chk_fail
.L46:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE12:
	.size	imprimePila, .-imprimePila
	.globl	navegadorDelMes
	.type	navegadorDelMes, @function
navegadorDelMes:
.LFB13:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$448, %rsp
	movq	%rdi, -328(%rbp)
	movq	%rsi, -336(%rbp)
	movq	%rdx, -344(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movl	$0, -316(%rbp)
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	call	creaPila
	movq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L48
	movq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, %rdx
	movq	-336(%rbp), %rcx
	movq	-328(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	navegadorDelMes
	movq	-344(%rbp), %rax
	leaq	-304(%rbp), %rdx
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	jmp	.L50
.L51:
	addl	$1, -316(%rbp)
	movq	-232(%rbp), %rdx
	leaq	-208(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	leaq	-304(%rbp), %rax
	addq	$72, %rax
	movq	%rax, %rdi
	call	pop
	leaq	-312(%rbp), %rax
	movq	-208(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-192(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-184(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-176(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-128(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movzwl	-120(%rbp), %edx
	movw	%dx, 88(%rsp)
	movq	%rax, %rdi
	call	push
.L50:
	movq	-232(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L51
	jmp	.L52
.L53:
	movq	-312(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	leaq	-304(%rbp), %rax
	leaq	72(%rax), %rdx
	movq	-112(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-104(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-96(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-88(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-80(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-72(%rbp), %rax
	movq	%rax, 40(%rsp)
	movq	-64(%rbp), %rax
	movq	%rax, 48(%rsp)
	movq	-56(%rbp), %rax
	movq	%rax, 56(%rsp)
	movq	-48(%rbp), %rax
	movq	%rax, 64(%rsp)
	movq	-40(%rbp), %rax
	movq	%rax, 72(%rsp)
	movq	-32(%rbp), %rax
	movq	%rax, 80(%rsp)
	movzwl	-24(%rbp), %eax
	movw	%ax, 88(%rsp)
	movq	%rdx, %rdi
	call	push
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	call	pop
.L52:
	movq	-312(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L53
	leaq	-312(%rbp), %rax
	movq	%rax, %rdi
	call	destruyePila
	movq	-336(%rbp), %rax
	movl	(%rax), %eax
	cmpl	-316(%rbp), %eax
	jge	.L54
	movq	-336(%rbp), %rax
	movl	-316(%rbp), %edx
	movl	%edx, (%rax)
	movq	-328(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rax)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rax)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rax)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rax)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rax)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rax)
	movq	-256(%rbp), %rdx
	movq	%rdx, 48(%rax)
	movq	-248(%rbp), %rdx
	movq	%rdx, 56(%rax)
	movq	-240(%rbp), %rdx
	movq	%rdx, 64(%rax)
	movq	-232(%rbp), %rdx
	movq	%rdx, 72(%rax)
	movq	-224(%rbp), %rdx
	movq	%rdx, 80(%rax)
.L54:
	leaq	-304(%rbp), %rdx
	leaq	-344(%rbp), %rax
	movq	-304(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	-296(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movq	-288(%rbp), %rcx
	movq	%rcx, 16(%rsp)
	movq	-280(%rbp), %rcx
	movq	%rcx, 24(%rsp)
	movq	-272(%rbp), %rcx
	movq	%rcx, 32(%rsp)
	movq	-264(%rbp), %rcx
	movq	%rcx, 40(%rsp)
	movq	-256(%rbp), %rcx
	movq	%rcx, 48(%rsp)
	movq	-248(%rbp), %rcx
	movq	%rcx, 56(%rsp)
	movq	-240(%rbp), %rcx
	movq	%rcx, 64(%rsp)
	movq	-232(%rbp), %rcx
	movq	%rcx, 72(%rsp)
	movq	-224(%rbp), %rcx
	movq	%rcx, 80(%rsp)
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	modifica
	movq	-344(%rbp), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, %rdx
	movq	-336(%rbp), %rcx
	movq	-328(%rbp), %rax
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	navegadorDelMes
.L48:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L55
	call	__stack_chk_fail
.L55:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE13:
	.size	navegadorDelMes, .-navegadorDelMes
	.section	.rodata
.LC21:
	.string	"%s\n"
.LC22:
	.string	"%s "
.LC23:
	.string	"***\n"
	.text
	.globl	imprimeEnArchivo
	.type	imprimeEnArchivo, @function
imprimeEnArchivo:
.LFB14:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$320, %rsp
	movq	%rdi, -216(%rbp)
	movq	%rsi, -224(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -8(%rbp)
	xorl	%eax, %eax
	movq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L56
	movq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	imprimeEnArchivo
	leaq	-208(%rbp), %rdx
	movq	-224(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	movl	$.LC4, %edi
	call	puts
	leaq	-208(%rbp), %rdx
	movq	-216(%rbp), %rax
	movl	$.LC21, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leaq	-208(%rbp), %rax
	leaq	50(%rax), %rdx
	movq	-216(%rbp), %rax
	movl	$.LC21, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	jmp	.L58
.L59:
	movq	-136(%rbp), %rdx
	leaq	-320(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	tope
	movq	-320(%rbp), %rax
	movq	%rax, -112(%rbp)
	movq	-312(%rbp), %rax
	movq	%rax, -104(%rbp)
	movq	-304(%rbp), %rax
	movq	%rax, -96(%rbp)
	movq	-296(%rbp), %rax
	movq	%rax, -88(%rbp)
	movq	-288(%rbp), %rax
	movq	%rax, -80(%rbp)
	movq	-280(%rbp), %rax
	movq	%rax, -72(%rbp)
	movq	-272(%rbp), %rax
	movq	%rax, -64(%rbp)
	movq	-264(%rbp), %rax
	movq	%rax, -56(%rbp)
	movq	-256(%rbp), %rax
	movq	%rax, -48(%rbp)
	movq	-248(%rbp), %rax
	movq	%rax, -40(%rbp)
	movq	-240(%rbp), %rax
	movq	%rax, -32(%rbp)
	movzwl	-232(%rbp), %eax
	movw	%ax, -24(%rbp)
	leaq	-112(%rbp), %rdx
	movq	-216(%rbp), %rax
	movl	$.LC22, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leaq	-112(%rbp), %rax
	leaq	30(%rax), %rdx
	movq	-216(%rbp), %rax
	movl	$.LC22, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leaq	-112(%rbp), %rax
	leaq	60(%rax), %rdx
	movq	-216(%rbp), %rax
	movl	$.LC21, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	leaq	-208(%rbp), %rax
	addq	$72, %rax
	movq	%rax, %rdi
	call	pop
.L58:
	movq	-136(%rbp), %rax
	movq	%rax, %rdi
	call	esVaciaP
	testl	%eax, %eax
	je	.L59
	movq	-216(%rbp), %rax
	movq	%rax, %rcx
	movl	$4, %edx
	movl	$1, %esi
	movl	$.LC23, %edi
	call	fwrite
	movq	-224(%rbp), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, %rdx
	movq	-216(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	imprimeEnArchivo
.L56:
	movq	-8(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L60
	call	__stack_chk_fail
.L60:
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE14:
	.size	imprimeEnArchivo, .-imprimeEnArchivo
	.globl	imprimeGrafo
	.type	imprimeGrafo, @function
imprimeGrafo:
.LFB15:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	subq	$48, %rsp
	movq	%rdi, -40(%rbp)
	movq	%rsi, -48(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	infoN
	movl	%eax, -12(%rbp)
	movq	-48(%rbp), %rax
	movq	%rax, %rdi
	call	infovertices
	movq	%rax, -8(%rbp)
	movl	$0, -20(%rbp)
	jmp	.L62
.L66:
	movl	-20(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	movl	$.LC21, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
	movl	$0, -16(%rbp)
	jmp	.L63
.L65:
	movl	-16(%rbp), %edx
	movl	-20(%rbp), %ecx
	movq	-48(%rbp), %rax
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	adyacente
	testl	%eax, %eax
	je	.L64
	movl	-16(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-8(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	movq	-40(%rbp), %rax
	movl	$.LC21, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	fprintf
.L64:
	addl	$1, -16(%rbp)
.L63:
	movl	-16(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jl	.L65
	movq	-40(%rbp), %rax
	movq	%rax, %rcx
	movl	$4, %edx
	movl	$1, %esi
	movl	$.LC23, %edi
	call	fwrite
	addl	$1, -20(%rbp)
.L62:
	movl	-20(%rbp), %eax
	cmpl	-12(%rbp), %eax
	jl	.L66
	leave
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE15:
	.size	imprimeGrafo, .-imprimeGrafo
	.section	.rodata
	.align 8
.LC24:
	.string	"No hay archivo para el grafo de empleados."
	.text
	.globl	CargaGrafo
	.type	CargaGrafo, @function
CargaGrafo:
.LFB16:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$504, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -392(%rbp)
	movq	%rsi, -400(%rbp)
	movq	%rdx, -408(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movl	$0, -372(%rbp)
	cmpq	$0, -392(%rbp)
	jne	.L68
	movl	$.LC24, %edi
	call	puts
	jmp	.L67
.L68:
	movl	$0, -372(%rbp)
	leaq	-304(%rbp), %rdx
	movq	-392(%rbp), %rax
	movl	$.LC7, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-240(%rbp), %rdx
	movq	-392(%rbp), %rax
	movl	$.LC10, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	movq	-408(%rbp), %rax
	movq	(%rax), %rax
	leaq	-112(%rbp), %rdx
	leaq	-304(%rbp), %rcx
	movq	%rcx, %rsi
	movq	%rax, %rdi
	call	buscanodo
	leaq	-112(%rbp), %rax
	addq	$80, %rax
	movq	%rax, %rdi
	call	inicializar
	leaq	-112(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-304(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-296(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-288(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-280(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-272(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-264(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-256(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	nuevoVertice
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-256(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	jne	.L71
	movq	-400(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-256(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	nuevoVertice
	jmp	.L71
.L74:
	leaq	-240(%rbp), %rdx
	movq	-392(%rbp), %rax
	movl	$.LC7, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-176(%rbp), %rdx
	movq	-392(%rbp), %rax
	movl	$.LC10, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	leaq	-240(%rbp), %rax
	movq	%rax, %rsi
	movl	$.LC9, %edi
	call	strcmp
	testl	%eax, %eax
	jne	.L72
	leaq	-240(%rbp), %rdx
	movq	-392(%rbp), %rax
	movl	$.LC10, %esi
	movq	%rax, %rdi
	movl	$0, %eax
	call	__isoc99_fscanf
	movl	$1, -372(%rbp)
	jmp	.L71
.L72:
	leaq	-240(%rbp), %rdx
	leaq	-368(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	leaq	-112(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-368(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-360(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-352(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-344(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-336(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-328(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-320(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	nuevoVertice
	movq	-32(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-360(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-352(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-344(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-336(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-328(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-320(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-32(%rbp), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-256(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	leaq	-112(%rbp), %rdx
	leaq	80(%rdx), %rcx
	movl	%ebx, %edx
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	nuevoArco
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-360(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-352(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-344(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-336(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-328(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-320(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	jne	.L73
	movq	-400(%rbp), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-360(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-352(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-344(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-336(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-328(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-320(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	nuevoVertice
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-360(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-352(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-344(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-336(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-328(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-320(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-256(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-400(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	nuevoArco
	jmp	.L71
.L73:
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-368(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-360(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-352(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-344(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-336(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-328(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-320(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-400(%rbp), %rax
	movq	(%rax), %rax
	movq	-304(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-296(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-288(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-280(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-272(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-264(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-256(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-400(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	nuevoArco
.L71:
	cmpl	$0, -372(%rbp)
	je	.L74
	leaq	-304(%rbp), %rdx
	movq	-408(%rbp), %rax
	movq	-112(%rbp), %rcx
	movq	%rcx, (%rsp)
	movq	-104(%rbp), %rcx
	movq	%rcx, 8(%rsp)
	movq	-96(%rbp), %rcx
	movq	%rcx, 16(%rsp)
	movq	-88(%rbp), %rcx
	movq	%rcx, 24(%rsp)
	movq	-80(%rbp), %rcx
	movq	%rcx, 32(%rsp)
	movq	-72(%rbp), %rcx
	movq	%rcx, 40(%rsp)
	movq	-64(%rbp), %rcx
	movq	%rcx, 48(%rsp)
	movq	-56(%rbp), %rcx
	movq	%rcx, 56(%rsp)
	movq	-48(%rbp), %rcx
	movq	%rcx, 64(%rsp)
	movq	-40(%rbp), %rcx
	movq	%rcx, 72(%rsp)
	movq	-32(%rbp), %rcx
	movq	%rcx, 80(%rsp)
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	modifica
	movq	-392(%rbp), %rax
	movq	%rax, %rdi
	call	feof
	testl	%eax, %eax
	je	.L68
	movq	-392(%rbp), %rax
	movq	%rax, %rdi
	call	fclose
.L67:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L75
	call	__stack_chk_fail
.L75:
	addq	$504, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE16:
	.size	CargaGrafo, .-CargaGrafo
	.section	.rodata
	.align 8
.LC25:
	.string	"Ese amigo ya esta en el grafo."
	.align 8
.LC26:
	.string	"Ese empleado no esta en el arbol."
	.text
	.globl	IntroduceAmigo
	.type	IntroduceAmigo, @function
IntroduceAmigo:
.LFB17:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -248(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-256(%rbp), %rdx
	leaq	-240(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-264(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-248(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-104(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-96(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	esMiembro
	testl	%eax, %eax
	je	.L77
	movq	-264(%rbp), %rdx
	leaq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	jne	.L78
	movq	-256(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-176(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-168(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-160(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-152(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-144(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-136(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-128(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	nuevoVertice
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-232(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-224(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-216(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-208(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-192(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movq	-256(%rbp), %rdx
	leaq	80(%rdx), %rcx
	movl	%ebx, %edx
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	nuevoArco
	jmp	.L76
.L78:
	movl	$.LC25, %edi
	call	puts
	jmp	.L76
.L77:
	movl	$.LC26, %edi
	call	puts
.L76:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L80
	call	__stack_chk_fail
.L80:
	addq	$360, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE17:
	.size	IntroduceAmigo, .-IntroduceAmigo
	.section	.rodata
	.align 8
.LC27:
	.string	"Ese empleado no esta en el grafo de amigos."
	.text
	.globl	EliminaAmigo
	.type	EliminaAmigo, @function
EliminaAmigo:
.LFB18:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$360, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -248(%rbp)
	movq	%rsi, -256(%rbp)
	movq	%rdx, -264(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-256(%rbp), %rdx
	leaq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-264(%rbp), %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-248(%rbp), %rax
	movq	-112(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-104(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-96(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-88(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-80(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-72(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movq	-64(%rbp), %rdx
	movq	%rdx, 48(%rsp)
	movq	-56(%rbp), %rdx
	movq	%rdx, 56(%rsp)
	movq	-48(%rbp), %rdx
	movq	%rdx, 64(%rsp)
	movq	-40(%rbp), %rdx
	movq	%rdx, 72(%rsp)
	movq	-32(%rbp), %rdx
	movq	%rdx, 80(%rsp)
	movq	%rax, %rdi
	call	esMiembro
	testl	%eax, %eax
	je	.L82
	movq	-264(%rbp), %rdx
	leaq	-240(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-232(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-224(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-216(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-208(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-192(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	je	.L83
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-240(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-232(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-224(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-216(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-208(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-200(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-192(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-256(%rbp), %rax
	movq	80(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movq	-256(%rbp), %rdx
	leaq	80(%rdx), %rcx
	movl	%ebx, %edx
	movl	%eax, %esi
	movq	%rcx, %rdi
	call	borrarArco
	movq	-256(%rbp), %rax
	leaq	80(%rax), %rdx
	movq	-240(%rbp), %rax
	movq	%rax, (%rsp)
	movq	-232(%rbp), %rax
	movq	%rax, 8(%rsp)
	movq	-224(%rbp), %rax
	movq	%rax, 16(%rsp)
	movq	-216(%rbp), %rax
	movq	%rax, 24(%rsp)
	movq	-208(%rbp), %rax
	movq	%rax, 32(%rsp)
	movq	-200(%rbp), %rax
	movq	%rax, 40(%rsp)
	movzwl	-192(%rbp), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	borrarVertice
	jmp	.L81
.L83:
	movl	$.LC27, %edi
	call	puts
	jmp	.L81
.L82:
	movl	$.LC26, %edi
	call	puts
.L81:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L85
	call	__stack_chk_fail
.L85:
	addq	$360, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE18:
	.size	EliminaAmigo, .-EliminaAmigo
	.globl	ActualizaGlobal
	.type	ActualizaGlobal, @function
ActualizaGlobal:
.LFB19:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$264, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -200(%rbp)
	movq	%rsi, -208(%rbp)
	movq	%fs:40, %rax
	movq	%rax, -24(%rbp)
	xorl	%eax, %eax
	movq	-208(%rbp), %rax
	movq	%rax, %rdi
	call	esVacio
	testl	%eax, %eax
	jne	.L86
	movq	-208(%rbp), %rax
	movq	%rax, %rdi
	call	izq
	movq	%rax, %rdx
	movq	-200(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	ActualizaGlobal
	leaq	-112(%rbp), %rdx
	movq	-208(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	info
	leaq	-112(%rbp), %rdx
	leaq	-176(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcpy
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	existeVertice
	testl	%eax, %eax
	jne	.L88
	movq	-200(%rbp), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	nuevoVertice
.L88:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	infovertices
	movq	%rax, -184(%rbp)
	movl	$0, -188(%rbp)
	jmp	.L89
.L92:
	movl	-188(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-184(%rbp), %rax
	addq	%rdx, %rax
	movq	%rax, %rdx
	leaq	-112(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	strcmp
	testl	%eax, %eax
	je	.L90
	movl	-188(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-184(%rbp), %rax
	addq	%rdx, %rax
	movq	-200(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	existeVertice
	testl	%eax, %eax
	jne	.L91
	movl	-188(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-184(%rbp), %rax
	addq	%rdx, %rax
	movq	-200(%rbp), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	nuevoVertice
	movl	-188(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-184(%rbp), %rax
	addq	%rdx, %rax
	movq	-200(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-200(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	nuevoArco
	jmp	.L90
.L91:
	movl	-188(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-184(%rbp), %rax
	addq	%rdx, %rax
	movq	-200(%rbp), %rdx
	movq	(%rdx), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ebx
	movq	-200(%rbp), %rax
	movq	(%rax), %rax
	movq	-176(%rbp), %rdx
	movq	%rdx, (%rsp)
	movq	-168(%rbp), %rdx
	movq	%rdx, 8(%rsp)
	movq	-160(%rbp), %rdx
	movq	%rdx, 16(%rsp)
	movq	-152(%rbp), %rdx
	movq	%rdx, 24(%rsp)
	movq	-144(%rbp), %rdx
	movq	%rdx, 32(%rsp)
	movq	-136(%rbp), %rdx
	movq	%rdx, 40(%rsp)
	movzwl	-128(%rbp), %edx
	movw	%dx, 48(%rsp)
	movq	%rax, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-200(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	nuevoArco
.L90:
	addl	$1, -188(%rbp)
.L89:
	movq	-32(%rbp), %rax
	movq	%rax, %rdi
	call	infoN
	cmpl	-188(%rbp), %eax
	jg	.L92
	movq	-208(%rbp), %rax
	movq	%rax, %rdi
	call	der
	movq	%rax, %rdx
	movq	-200(%rbp), %rax
	movq	%rdx, %rsi
	movq	%rax, %rdi
	call	ActualizaGlobal
.L86:
	movq	-24(%rbp), %rax
	xorq	%fs:40, %rax
	je	.L93
	call	__stack_chk_fail
.L93:
	addq	$264, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE19:
	.size	ActualizaGlobal, .-ActualizaGlobal
	.globl	empleadoPopular
	.type	empleadoPopular, @function
empleadoPopular:
.LFB20:
	.cfi_startproc
	pushq	%rbp
	.cfi_def_cfa_offset 16
	.cfi_offset 6, -16
	movq	%rsp, %rbp
	.cfi_def_cfa_register 6
	pushq	%rbx
	subq	$120, %rsp
	.cfi_offset 3, -24
	movq	%rdi, -56(%rbp)
	movq	%rsi, -64(%rbp)
	movl	$0, -36(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	infoN
	movl	%eax, -28(%rbp)
	movq	-56(%rbp), %rax
	movq	%rax, %rdi
	call	infovertices
	movq	%rax, -24(%rbp)
	movl	$0, -44(%rbp)
	jmp	.L95
.L100:
	movl	$0, -32(%rbp)
	movl	$0, -40(%rbp)
	jmp	.L96
.L99:
	movl	-44(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movq	-56(%rbp), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ebx
	movl	-40(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rdx, %rax
	movq	-56(%rbp), %rdx
	movq	(%rax), %rcx
	movq	%rcx, (%rsp)
	movq	8(%rax), %rcx
	movq	%rcx, 8(%rsp)
	movq	16(%rax), %rcx
	movq	%rcx, 16(%rsp)
	movq	24(%rax), %rcx
	movq	%rcx, 24(%rsp)
	movq	32(%rax), %rcx
	movq	%rcx, 32(%rsp)
	movq	40(%rax), %rcx
	movq	%rcx, 40(%rsp)
	movzwl	48(%rax), %eax
	movw	%ax, 48(%rsp)
	movq	%rdx, %rdi
	call	posicion
	movl	%eax, %ecx
	movq	-56(%rbp), %rax
	movl	%ebx, %edx
	movl	%ecx, %esi
	movq	%rax, %rdi
	call	adyacente
	testl	%eax, %eax
	je	.L97
	addl	$1, -32(%rbp)
.L97:
	movl	-32(%rbp), %eax
	cmpl	-36(%rbp), %eax
	jle	.L98
	movl	-32(%rbp), %eax
	movl	%eax, -36(%rbp)
	movl	-44(%rbp), %eax
	movslq	%eax, %rdx
	movq	%rdx, %rax
	salq	$2, %rax
	addq	%rdx, %rax
	leaq	0(,%rax,4), %rdx
	addq	%rdx, %rax
	addq	%rax, %rax
	movq	%rax, %rdx
	movq	-24(%rbp), %rax
	addq	%rax, %rdx
	movq	-64(%rbp), %rax
	movq	(%rdx), %rcx
	movq	%rcx, (%rax)
	movq	8(%rdx), %rcx
	movq	%rcx, 8(%rax)
	movq	16(%rdx), %rcx
	movq	%rcx, 16(%rax)
	movq	24(%rdx), %rcx
	movq	%rcx, 24(%rax)
	movq	32(%rdx), %rcx
	movq	%rcx, 32(%rax)
	movq	40(%rdx), %rcx
	movq	%rcx, 40(%rax)
	movzwl	48(%rdx), %edx
	movw	%dx, 48(%rax)
.L98:
	addl	$1, -40(%rbp)
.L96:
	movl	-40(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L99
	addl	$1, -44(%rbp)
.L95:
	movl	-44(%rbp), %eax
	cmpl	-28(%rbp), %eax
	jl	.L100
	addq	$120, %rsp
	popq	%rbx
	popq	%rbp
	.cfi_def_cfa 7, 8
	ret
	.cfi_endproc
.LFE20:
	.size	empleadoPopular, .-empleadoPopular
	.ident	"GCC: (Ubuntu/Linaro 4.7.3-1ubuntu1) 4.7.3"
	.section	.note.GNU-stack,"",@progbits
