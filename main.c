/* 
 *
 * Author: César Landesa Gómez
 *
 * Created on 2 de octubre de 2013, 9:34
 * 
 */

#include <stdio.h>
#include <stdlib.h>
#include "abb.h"

/*
 * Este programa sirve para realizar la gestion de las visitas web de los empleados 
 * de una empresa.
 */



int main(int argc, char** argv) {


    int opc, opc1, opc2, opc3, num, max = 0;
    abb A;
    char apnom[50];
    tipoelem aux, empleadoActual, navMes;
    tipovertice auxx;
    FILE *archivo;
    FILE *arc;
    grafo GLOBAL;
    inicializar(&GLOBAL);
    crea(&A);
    arc = fopen("grafoEmpleados.txt", "r");
    archivo = fopen("listaEmpleados.txt", "r");
    CargaArchivo(archivo, &A);
    CargaGrafo(arc, &GLOBAL, &A);



    do {
        printf("-----MENU PRINCIPAL-----\n");
        printf("1) MENU OPERADOR.\n");
        printf("2) MENU JEFE.\n");
        printf("3) MENU EMPLEADO.\n");
        printf("0) Salir\n");
        printf("Introduzca una opcion: ");
        scanf("%d", &opc);
        getchar();



        if (opc == 1) {
            do {
                printf("-----MENU OPERADOR-----\n");
                printf("1) Introduccion manual de nuevos accesos web.\n");
                printf("2) Eliminacion de empleados de la empresa.\n");
                printf("3) Acualizacion de la red social GLOBAL de la empresa.\n");
                printf("4) Imprimir el grafo GLOBAL.\n");
                printf("0) Volver al menu principal\n");
                printf("Introduce una opcion: ");
                scanf("%d", &opc1);
                getchar();

                switch (opc1) {

                    case 1:

                        printf("Introduce el nombre del empleado:\n");
                        gets(apnom);
                        Intro(&A, apnom);
                        break;

                    case 2:
                        printf("Introduce el nombre del empleado:\n");
                        gets(apnom);
                        Del(&A, &GLOBAL, apnom);
                        break;
                    case 3:

                        printf("Actualizando red social GLOBAL...\n");
                        ActualizaGlobal(&GLOBAL, A);
                        printf("Actualizada!\n");
                        break;

                    case 4:
                        printf("----Grafo global:----\n");
                        imprimirGrafo(GLOBAL);
                        break;
                    case 0:
                        printf("Volviendo al menu principal...\n");
                        break;
                    default:
                        printf("Opcion incorrecta\n");
                        break;

                }
            } while (opc1 != 0);

        } else if (opc == 3) {
            do {
                printf("Introduzca su nombre:\n");

                gets(apnom);
                strcpy(aux.nombre, apnom);
                if (esMiembro(A, aux)) {
                    buscanodo(A, apnom, &empleadoActual);

                } else {
                    printf("Ese empleado no está en el arbol.\n");
                }
            } while (!esMiembro(A, aux));

            do {
                printf("\n");
                printf("-----MENU EMPLEADO-----\n");
                printf("1) Introducir amigos.\n");
                printf("2) Eliminar amigo.\n");
                printf("3) Imprimir grafo de amigos del empleado\n");
                printf("0) Volver al menu principal.\n");
                printf("Introduce una opcion: ");
                scanf("%d", &opc2);
                getchar();

                switch (opc2) {

                    case 1:
                        printf("Introduzca el nombre de su amigo:\n");
                        gets(apnom);
                        IntroduceAmigo(A, &empleadoActual, apnom);
                        break;

                    case 2:
                        printf("Introduzca el nombre del amigo a eliminar:\n");
                        gets(apnom);
                        EliminaAmigo(A, &empleadoActual, apnom);
                        break;
                    case 3:
                        imprimirGrafo(empleadoActual.amigos);
                        break;
                    case 0:
                        printf("Saliendo al menu principal\n");
                        break;
                    default:
                        printf("Opcion incorrecta\n");
                        break;

                }
            } while (opc2 != 0);

        } else if (opc == 2) {


            do {
                printf("\n");
                printf("-----MENU JEFE-----\n");
                printf("1) Listado de empleados\n");
                printf("2) Consulta rapida.\n");
                printf("3) Consulta extendida.\n");
                printf("4) Consulta de datos por URL.(Visitas a micuenta.bancobancario.com)\n");
                printf("5) Navegador del mes.\n");
                printf("6) Lista de amigos de un empleado.\n");
                printf("7) Empleado mas popular.\n");
                printf("0) Volver al meu principal.\n\n");
                printf("Introduce una opcion: ");
                scanf("%d", &opc3);
                getchar();

                switch (opc3) {

                    case 1:
                        inordengali(A);
                        break;

                    case 2:
                        printf("Introduce el nombre:\n");
                        gets(apnom);
                        strcpy(aux.nombre, apnom);

                        if (esMiembro(A, aux) == 1) {
                            buscanodo(A, apnom, &aux);
                            ImprimeEmpleado(aux);
                        } else {
                            printf("El empleado no esta registrado...");
                        }
                        break;

                    case 3:

                        printf("Introduce el nombre:\n");
                        gets(apnom);
                        strcpy(aux.nombre, apnom);
                        printf("Introduce el numero de urls a imprimir: ");
                        scanf("%d", &num);
                        if (esMiembro(A, aux) == 1) {
                            buscanodo(A, apnom, &aux);
                            ImprimeEmpleadoAvanzado(aux, num);
                        } else {
                            printf("El empleado no esta registrado...");
                        }

                        break;
                    case 4:
                        printf("Lista de empleados que coinciden con la busqueda:\n");
                        inordenurl(A);
                        break;

                    case 5:

                        navegadorDelMes(&navMes, &max, A);
                        printf("---NAVEGADOR DEL MES---\n");
                        ImprimeEmpleado(navMes);
                        break;


                    case 6:
                        printf("Introduce el nombre:\n");
                        gets(apnom);
                        strcpy(aux.nombre, apnom);

                        if (esMiembro(A, aux) == 1) {
                            buscanodo(A, apnom, &aux);
                            imprimirGrafo(aux.amigos);
                        } else {
                            printf("El empleado no esta registrado...");
                        }
                        break;
                    case 7:

                        empleadoPopular(GLOBAL, &auxx);
                        printf("---EMPLEADO MAS POPULAR---\n");
                        printf("%s", auxx.nombre);

                        break;
                    case 0:
                        printf("Saliendo al menu principal\n");
                        break;
                    default:
                        printf("Opcion incorrecta\n");
                        break;


                }
            } while (opc3 != 0);
        } else {
            if (opc != 0) {
                printf("Opcion incorrecta.\n");
            }
        }


    } while (opc != 0);

    arc = fopen("grafoEmpleados.txt", "w");
    ActualizaGlobal(&GLOBAL, A);
    imprimeGrafo(arc, GLOBAL);
    archivo = fopen("listaEmpleados.txt", "w");
    imprimeEnArchivo(archivo, A);
    destruye(&A);
    fclose(archivo);
}

