/* 
 * File:   funciones.h
 * Author: mjose
 *
 * Created on 18 de octubre de 2013, 13:09
 */

#ifndef FUNCIONES_H
#define	FUNCIONES_H

#ifdef	__cplusplus
extern "C" {
#endif

#include "grafo.h"
    //FUNCIONES DEL PROGRAMA DE PRUEBA DE GRAFOS
    //Opción a del menú, introducir un vertice en el grafo
    void introducirVertice(grafo *G);
    //Opción b del menú, eliminar un vértice del grafo
    void eliminarVertice(grafo *G);
    //Opción c del menú, crear una relación entre dos vértices
    void crearArco(grafo *G);
    //Opción d del menú, eliminar una relación entre dos vértices
    void eliminarArco(grafo *G);
    //Opción i del menú, imprimir el grafo
    void imprimirGrafo(grafo G);

#ifdef	__cplusplus
}
#endif

#endif	/* FUNCIONES_H */

