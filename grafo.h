#define MAXVERTICES 100 /*maximo numero de nodos*/

//Información que se almacena en cada vértice

typedef struct {
    char nombre[50];

} tipovertice;

typedef void * grafo; //tipo opaco, los detalles de implementación están ocultos

//Creación del grafo con 0 nodos
void inicializar(grafo *G);

//ESTA FUNCIÓN HAY QUE MODIFICARLA EN grafo.c SI SE CAMBIA EL TIPO DE DATO tipovertice
//PARA QUE TENGA EN CUENTA tipovertice A LA HORA DE BUSCAR LA POSICIÓN Vert en el vector VERTICES
//Devuelve la posición del vértice Vert en el vector VERTICES del grafo G
//Si devuelve -1 es porque no encontró el vértice
int posicion(grafo G, tipovertice Vert);
///////////////////////////////////////////////////////////////////////////////

//Devuelve 1 si el grafo G existe y 0 en caso contrario
int existe(grafo G);

//Devuelve 1 si el vértice Vert existe en el grafo G
int existeVertice(grafo G, tipovertice Vert);

//Inserta un vértice en el grafo
void nuevoVertice(grafo *G, tipovertice Vert);

//Borra un vértice del grafo
void borrarVertice(grafo *G, tipovertice Vert);

//Crea el arco de relación entre VERTICES(pos1) y VERTICES(pos2)
void nuevoArco(grafo *G, int pos1, int pos2);

//Borra el arco de relación entre VERTICES(pos1) y VERTICES(pos2)
void borrarArco(grafo *G, int pos1, int pos2);

//Devuelve 1 si VERTICES(pos1) y VERTICES(pos2) son vértices adyacentes
int adyacente(grafo G, int pos1, int pos2);

//Destruye el grafo
void borrarGrafo(grafo *G);

//Devuelve el número de vértices del grafo G
int infoN(grafo G);

//Devuelve el vector de vértices VERTICES del grafo G
tipovertice* infovertices(grafo G);
