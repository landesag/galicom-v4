#include <stdio.h>
#include <stdlib.h>
#include "grafo.h"

//FUNCIONES DEL PROGRAMA DE PRUEBA DE GRAFOS

//Opción a del menú, introducir un vertice en el grafo

void introducirVertice(grafo *G) {
    tipovertice v1;
    printf("Introduce vertice: ");
    gets(v1.nombre);
    if (existeVertice(*G, v1))
        printf("Ese vertice ya esta en el grafo\n");
    else
        nuevoVertice(G, v1);
}
//Opción b del menú, eliminar un vértice del grafo

void eliminarVertice(grafo *G) {
    tipovertice v1;
    printf("Introduce vertice: ");
    gets(v1.nombre);
    if (existeVertice(*G, v1))
        borrarVertice(G, v1);
    else
        printf("Ese vertice no existe en el grafo\n");
}

//Opción c del menú, crear una relación entre dos vértices

void crearArco(grafo *G) {
    tipovertice v1, v2;
    //Insertamos una nueva relación pidiendo los datos al usuario controlando que existan los vértices
    printf("Nueva relacion vertice1-->vertice2\n");
    //Vértice origen del arco
    do {
        printf("Introduce vertice origen: ");
        gets(v1.nombre);
    } while (!existeVertice(*G, v1));
    //Vértice destino del arco
    do {
        printf("Introduce vertice destino: ");
        gets(v2.nombre);
    } while (!existeVertice(*G, v2));
    //Creación del arco
    if (adyacente(*G, posicion(*G, v1), posicion(*G, v2)))
        printf("Ya existe esa relacion\n");
    else
        nuevoArco(G, posicion(*G, v1), posicion(*G, v2));
}

//Opción d del menú, eliminar una relación entre dos vértices

void eliminarArco(grafo *G) {
    tipovertice v1, v2;
    //Eliminamos una relación pidiendo los datos al usuario controlando que existan los vértices
    printf("Eliminar relacion vertice1-->vertice2\n");
    //Vértice origen del arco
    do {
        printf("Introduce vertice origen: ");
        gets(v1.nombre);
    } while (!existeVertice(*G, v1));
    //Vértice destino del arco
    do {
        printf("Introduce vertice destino: ");
        gets(v2.nombre);
    } while (!existeVertice(*G, v2));
    //Eliminación del arco
    if (adyacente(*G, posicion(*G, v1), posicion(*G, v2)))
        borrarArco(G, posicion(*G, v1), posicion(*G, v2));
    else
        printf("No existe esa relacion\n");
}

//Opción i del menú, imprimir el grafo
//Función que imprime el grafo utilizando infoN para saber cuántos vértices tiene
//y infovertices para recuperar el vector de vértices y recorrerlo

void imprimirGrafo(grafo G) {
    tipovertice *VECTOR; //Para almacenar el vector de vértices del grafo
    int N; //número de vértices del grafo

    //Para recorrerla, simplemente vamos a recorrer la matriz de adyacencia
    N = infoN(G);
    VECTOR = infovertices(G);

    int i, j;
    printf("El grafo actual es:\n");
    for (i = 0; i < N; i++) {
        //Imprimo el vértice
        printf("Vertice(%d): %s\n", i, (VECTOR[i]).nombre);
        //Chequeo sus arcos
        for (j = 0; j < N; j++)
            if (adyacente(G, i, j))
                printf("\t%s-->%s\n", VECTOR[i].nombre, VECTOR[j].nombre);
    }
}

