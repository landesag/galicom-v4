#include <stdlib.h>

////////////////////////////////////////////////////////////

struct datosUrl {
    char web[30];
    char fecha[30];
    char hora[30];
};

typedef struct datosUrl tipoelempila;
////////////////////////////////////////////////////////////

struct tipo_celda {
    tipoelempila elemento;
    struct tipo_celda * sig;
};

typedef struct tipo_celda * pila;

void creaPila(pila *P) {
    *P = NULL;
}

void destruyePila(pila *P) {
    pila aux;
    aux = *P;
    while (aux != NULL) {
        aux = aux->sig;
        free(*P);
        *P = aux;
    }
}

unsigned esVaciaP(pila P) {
    return P == NULL;
}

tipoelempila tope(pila P) {
    if (!esVaciaP(P)) /*si pila no vacia*/
        return P->elemento;
}

void push(pila *P, tipoelempila E) {
    pila aux;
    aux = (pila) malloc(sizeof (struct tipo_celda));
    aux->elemento = E;
    aux->sig = *P;
    *P = aux;
}

void pop(pila *P) {
    pila aux;
    if (!esVaciaP(*P)) /*si pila no vacia*/ {
        aux = *P;
        *P = (*P)->sig;
        free(aux);
    }
}

