#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pila.h"
#include "grafo.h"
////////////////////////////////////////////////////////////

struct Miembro {
    char nombre[50];
    char nif[15];
    pila url;
    grafo amigos;
};

typedef struct Miembro tipoelem;
typedef char * tipoclave;

////////////////////////////////////////////////////////////////

struct celda {
    tipoelem info;
    struct celda *izq, *der;
};

typedef struct celda * abb;

void crea(abb *A) {
    *A = NULL;
}

void destruye(abb *A) {
    if (*A != NULL) {
        destruye(&(*A)->izq);
        destruye(&(*A)->der);
        free(*A);
        *A = NULL;
    }
}

unsigned esVacio(abb A) {
    return A == NULL;
}

void inserta(abb *A, tipoelem E) {
    if (esVacio(*A)) {
        *A = (abb) malloc(sizeof (struct celda));
        (*A)->info = E;
        (*A)->izq = NULL;
        (*A)->der = NULL;
    } else if (strcmp(E.nombre, ((*A)->info).nombre) < 0)
        inserta(&(*A)->izq, E);
    else
        inserta(&(*A)->der, E);
}

tipoelem suprime_min(abb *A) {
    abb aux;
    tipoelem ele;
    if ((*A)->izq == NULL) {
        ele = (*A)->info;
        aux = *A;
        *A = (*A)->der;
        free(aux);
        return ele;
    } else
        return suprime_min(&(*A)->izq);

}

void suprime(abb *A, tipoelem E) {
    abb aux;
    if (*A != NULL) {
        if (strcmp(E.nombre, ((*A)->info).nombre) < 0)
            suprime(&(*A)->izq, E);
        else if (strcmp(E.nombre, ((*A)->info).nombre) > 0)
            suprime(&(*A)->der, E);
        else if ((*A)->izq == NULL && (*A)->der == NULL) {
            free(*A);
            *A = NULL;
        } else if ((*A)->izq == NULL) {
            aux = *A;
            *A = (*A)->der;
            free(aux);
        } else if ((*A)->der == NULL) {
            aux = *A;
            *A = (*A)->izq;
            free(aux);
        } else
            (*A)->info = suprime_min(&(*A)->der);

    }
}

unsigned esMiembro(abb A, tipoelem E) {
    if (esVacio(A))
        return 0;
    else if (strcmp(E.nombre, (A->info).nombre) == 0)
        return 1;
    else if (strcmp(E.nombre, (A->info).nombre) > 0)
        return esMiembro(A->der, E);
    else
        return esMiembro(A->izq, E);
}

void info(abb A, tipoelem *E) {
    *E = A->info;
}

abb izq(abb A) {
    return A->izq;
}

abb der(abb A) {
    return A->der;
}

void buscanodo(abb A, tipoclave cl, tipoelem *nodo) {
    if (esVacio(A))
        printf("Clave inexistente\n");
    else if (strcmp(cl, (A->info).nombre) == 0)
        *nodo = A->info;
    else if (strcmp(cl, (A->info).nombre) < 0)
        buscanodo(izq(A), cl, nodo);
    else
        buscanodo(der(A), cl, nodo);
}

void modifica(abb *A, tipoclave cl, tipoelem nodo) {
    if (esVacio(*A)) {
        printf("Clave inexistente\n");
    } else if (strcmp(cl, ((*A)->info).nombre) == 0)
        (*A)->info = nodo;
    else if (strcmp(cl, ((*A)->info).nombre) < 0)
        modifica(&(*A)->izq, cl, nodo);
    else
        modifica(&(*A)->der, cl, nodo);
}
