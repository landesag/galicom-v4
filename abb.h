#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "pila.h"
#include "grafo.h"

////////////////////////////////////////////////////////////////

struct Miembro {
    char nombre[50];
    char nif[15];
    pila url;
    grafo amigos;
};

typedef struct Miembro tipoelem;
typedef char * tipoclave;

////////////////////////////////////////////////////////////////

typedef void *abb; //tipo opaco

void crea(abb *A);
void destruye(abb *A);
unsigned esVacio(abb A);
void inserta(abb *A, tipoelem E);
tipoelem suprime_min(abb *A);
void suprime(abb *A, tipoelem E);
unsigned esMiembro(abb A, tipoelem E);
abb izq(abb A);
abb der(abb A);
void info(abb A, tipoelem *E);
void buscanodo(abb A, tipoclave cl, tipoelem *nodo);
void modifica(abb *A, tipoclave cl, tipoelem nodo);
